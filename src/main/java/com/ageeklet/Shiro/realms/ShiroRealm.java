package com.ageeklet.Shiro.realms;

import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.realm.SimpleAccountRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;

import java.util.HashSet;
import java.util.Set;

public class ShiroRealm extends AuthorizingRealm{

    //认证需要实现的方法
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //1:把AuthenticationToken转化为UsernamePasswordToken
        UsernamePasswordToken upToken = (UsernamePasswordToken) authenticationToken;
        //2：从UsernamePasswordToken对象中获取username
        String username = upToken.getUsername();
        //3：调用数据库的方法，从数据库中查询username对应的记录
        System.out.println("从数据库中获取Username："+username+"所对应的信息");
        //4：若用户不存在，则可以抛出UnknownAccountException异常
        if ("unknown".equals(username)){
            throw new UnknownAccountException("用户不存在！");
        }
        //5：根据用户信息的情况，决定是否需要抛出其他AuthenticationException 异常
        if("monster".equals(username)){
            throw new LockedAccountException("用户被锁定！");
        }
        //6：根据用户的情况，来构建AuthenticationInfo 对象并返回,通常使用的实现类SimpleAuthenticationInfo
        //一下信心是从数据库中获取的
        //1：principal:认证的实体信息，可以是Username，也可以的数据表中对应的实体类对象
        Object principal = username;
        //2:credentials:密码
        Object credentials = null;
        if ("admin".equals(username)){
            credentials = "038bdaf98f2037b31f1e75b5b4c9b26e";
        }else if ("user".equals(username)){
            credentials = "098d2c478e9c11555ce2823231e02ec1";
        }
        //3:realmName:当前realm的name，调用父类的getName()方法即可
        String realmName = getName();
        //4:salt：盐值
        ByteSource credentialsSalt = ByteSource.Util.bytes(username);

        SimpleAuthenticationInfo info = null;
        info = new SimpleAuthenticationInfo(principal,credentials,credentialsSalt,realmName);

        return info;
    }

    public static void main(String[] args){
        String md5 = "MD5";
        Object credentials = "123456";
        Object salt = ByteSource.Util.bytes("user");
        int hasIterations = 1024;
        Object result = new SimpleHash(md5,credentials,salt,hasIterations);
        System.out.println(result);
    }


    //授权需要实现的方法
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //1 : PrincipalCollection中获取登录用户的信息
        Object principal = principalCollection.getPrimaryPrincipal();
        //2 : 利用登陆的用户信息来授予当前用户的角色或者权限
        Set<String> roles = new HashSet<>();
        roles.add("user");
        if ("admin".equals(principal)){
            roles.add("admin");
        }
        //3 : 创建SimpleAuthenticationInfo对象，并设置reles属性的值
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(roles);
        //4 : 返回SimpleAuthenticationInfo对象
        return info;
    }

}
