package com.ageeklet.Shiro.handlers;

import com.ageeklet.Shiro.services.ShiroService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/shiro")
public class ShiroHandler {

    @Autowired
    private ShiroService shiroService;

    @RequestMapping("/testAnnotation")
    public String testAnnotation(HttpSession session){
        session.setAttribute("key","hello shiro");
        shiroService.testAnnotation();
        return "redirect:/list.jsp";
    }


    @RequestMapping("/login")
    public String login(@RequestParam("username")String username,@RequestParam("password")String password){
        Subject currentUser = SecurityUtils.getSubject();
        if (!currentUser.isAuthenticated()){
            //吧用户民和密码封装伟为UsernamePasswordToken对象
            UsernamePasswordToken token = new UsernamePasswordToken(username,password);
            token.setRememberMe(true);
            try {
                //执行登录
                currentUser.login(token);
            }catch (AuthenticationException e){
                //所有认证时的异常
                System.out.println("登录失败："+e.getMessage());
            }
        }
        return "redirect:/list.jsp";
    }

}
