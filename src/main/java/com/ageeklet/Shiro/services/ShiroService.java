package com.ageeklet.Shiro.services;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.session.Session;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ShiroService {

    @RequiresRoles({"admin"})
    public void testAnnotation(){
        System.out.println(new Date());
        Session session = SecurityUtils.getSubject().getSession();
        String result = (String) session.getAttribute("key");
        System.out.println("session value:"+result);
    }

}
