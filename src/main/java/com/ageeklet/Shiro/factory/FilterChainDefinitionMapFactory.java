package com.ageeklet.Shiro.factory;

import java.util.LinkedHashMap;

public class FilterChainDefinitionMapFactory {

    /**
     * 权限集合
     * @return
     */
    public LinkedHashMap<String,String> buildFilterChainDefinitionFactory(){
        LinkedHashMap<String,String> map = new LinkedHashMap<String,String>();
/*
                 /login.jsp = anon
                /shiro/login = anon
                /shiro/logout = logout
                /user.jsp = roles[user]
                /admin.jsp = roles[admin]

                /** = authc

*/

        map.put("/login.jsp","anon");
        map.put("/shiro/login","anon");
        map.put("/shiro/logout","logout");
        map.put("/user.jsp","authc,roles[user]");
        map.put("/admin.jsp","authc,roles[admin]");
        map.put("list.jsp","user");


        map.put("/**","authc");
        return map;
    }



}
