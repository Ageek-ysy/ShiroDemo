<%--
  Created by IntelliJ IDEA.
  User: ageeklet
  Date: 18-3-29
  Time: 下午3:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <h1>list.jsp</h1>

    welcome: <shiro:principal></shiro:principal>

    <shiro:hasRole name="admin">
    <br><br>
    <a href="admin.jsp">Admin Page</a>
    </shiro:hasRole>

    <shiro:hasRole name="user">
    <br><br>
    <a href="user.jsp">User Page</a>
    </shiro:hasRole>
    
    <br><br>
    <a href="/shiro/testAnnotation">Test Annotation</a>

    <br><br>
    <a href="shiro/logout">logout</a>

    

</body>
</html>
